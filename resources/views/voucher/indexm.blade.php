<div class="my-5">
    <table class="table">
        <thead>
            <tr>
                <th>No.</th>
                <th>NIK</th>
                <th>Name</th>
                <th>Kendaraan</th>
                <th>Plat</th>
                <th>Jarak</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($vouchers as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->nik }}</td>
                    <td>{{ $item->name }}</td>
                    <td>
                        @foreach ($item->kendaraan as $kendaraan)
                            {{ $kendaraan->name }} <br>
                        @endforeach
                    </td>
                    <td>{{ $item->plat }}</td>
                    <td>{{ $item->jarak }}</td>
                    <td><a href="/book-edit/{{ $item->slug }}">edit</a>
                        <a href="/book-delete/{{ $item->slug }}">delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>