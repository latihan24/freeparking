<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Autoroad - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('parkingt/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('parkingt/css/animate.css') }}">
    
    <link rel="stylesheet" href="{{ asset('parkingt/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('parkingt/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('parkingt/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('parkingt/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('parkingt/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('parkingt/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('parkingt/css/jquery.timepicker.css') }}">

    
    <link rel="stylesheet" href="{{ asset('parkingt/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('parkingt/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('parkingt/css/style.css') }}">
  </head>
  <body>
    
	  @include('partial.nav')
    <!-- END nav -->
	
	@yield('content')
	
    
    

    





   
    





    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
       
        
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader 
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
-->

  <script src="{{ asset('parkingt/js/jquery.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/popper.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('parkingt/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/aos.js') }}"></script>
  <script src="{{ asset('parkingt/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('parkingt/js/jquery.timepicker.min.js') }}"></script>
  <script src="{{ asset('parkingt/js/scrollax.min.js') }}"></script>

  <script src="{{ asset('parkingt/js/main.js') }}"></script>
    
  </body>
</html>