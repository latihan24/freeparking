<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_k', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('voucher_id');
        $table->foreign('voucher_id')->references('id')->on('vouchers');
        $table->unsignedBigInteger('kendaraan_id');
        $table->foreign('kendaraan_id')->references('id')->on('kendaraans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_k');
    }
};