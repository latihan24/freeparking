<?php

namespace App\Models;

use App\Models\User;
use App\Models\Kendaraan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Voucher extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function kendaraan(): BelongsToMany
    {
        return $this ->belongsToMany(Kendaraan::class, 'v_k', 'voucher_id', 'kendaraan_id');
    }
    
    
}
