<?php

namespace Database\Seeders;

use App\Models\Kendaraan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class KendaraanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Kendaraan::truncate();
        Schema::enableForeignKeyConstraints();

        $data= [
            'mobil','motor'
        ];

        foreach ($data as $value) {
            Kendaraan::insert([
                'name' => $value
            ]);
        }
    }
}
