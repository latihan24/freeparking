@extends('layouts.main')

@section('content')
<div >

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
    integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
    integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>

    <link rel="stylesheet" href="{{ asset('leaflet/leaflet-routing-machine.css') }}"/>
    <script src="{{ asset('leaflet/leaflet-routing-machine.min.js') }}"></script>
    <script src="{{ asset('leaflet/Control.Geocoder.js') }}"></script>

    <script src="{{ asset('leaflet/config.js') }}"></script>

    <style>
        #map {
            height: 500px;
        }
    </style>
    <div id="map"></div>
<script>
    
    var map = L.map('map').setView([-6.9772027, 107.6597198], 18);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//ROuteMechine
var waypoints =[
    L.latLng(-6.152702328013234, 106.56780450959889),
    L.latLng(-6.152692239244786, 106.56926268579457)
];
var geocoder= L.Control.Geocoder.nominatim()
	
var routeControl = L.Routing.control({
    geocoder: geocoder,
    waypoints: waypoints,
    routewhileDragging: true,
    lineOptions: {
        styles: [{ color:'green', opacity: 1, weight:5}]
    },
}).addTo(map);

routeControl.on('routesfound', function (e)  {
    console.log(e.routes[0]);
    
    var distance = e.routes[0].summary.totalDistance;
    var time = e.routes[0].summary.totalTime;

    document.getElementById("jarak").value =e.routes[0].summary.totalDistance;

});

</script>
<div class="mx-5 mt-5 w-50">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="voucher-add" method="post" >
        @csrf
        <div class="mt-3 mb-3">
            <label for="nik" class="form-label">NIK</label>
            <input type="text" name="nik" id="nik" class="form-control" placeholder="nik" value="{{ old('nik') }}">
        </div>

        <div class="mb-3">
            <label for="name" class="form-label">Nama</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="nama" value="{{ old('nama') }}">
        </div>

        <div class="mb-3">
            <label for="kendaraan" class="form-label">Kendaraan</label>
            <select name="kendaraans[]" id="kendaraan" class="form-control select-multiple" >
                @foreach ($kendaraans as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="plat" class="form-label">No Plat</label>
            <input type="text" name="plat" id="plat" class="form-control" placeholder="nomor plat kendaraan"
            value="{{ old('plat') }}">
        </div>

        <div class="mb-3">
            <input  name="jarak" id="jarak" type="hidden" class="form-control" >
        </div>

        
        <div class="mt-3 ">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </form>
</div>
</div>
<br><br><br><br><br><br><br>
@endsection
