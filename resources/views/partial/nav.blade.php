<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="/"><span>Freeparking</span></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          @guest
          <li class="nav-item active"><a href="{{ route('login') }}"class="nav-link">{{ __('Login') }}</a></li>
          <li class="nav-item"><a href="{{ route('register') }}"class="nav-link">{{ __('Register') }}</a></li>
          @endguest
          @auth
          <li class="nav-item"><a class="nav-link" href="/voucher-add"  @if(request()->route()->uri=='/voucher-add') @endif>My Voucher</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" >
             {{ __('Logout') }}
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
             @csrf
         </form></li> 
          @endauth
          
          
      </div>

    </div>
  </nav>