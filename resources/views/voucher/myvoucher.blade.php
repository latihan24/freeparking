<div class="my-5">
    <div class="row">
        @foreach ($welan as $item)
        <div class="col-lg-3 col-md-4 col-sm-6 mb-3">
            
                <div class="card h-100 bodyas">
                    <a href="/welan/{{ $item->slug }}">
                <img src="{{ $item->cover !=null ? asset('storage/cover/'.$item->cover) : asset('image/nosatu.jpg') }}"
                    class="card-img-top" draggable="false" width="255" height="150">
                <div class="card-body">
                    <h5 class="card-title">{{ $item->name }}</h5>
                    <p class="card-text">
                        @foreach ($item->frameworks as $framework)
                                {{ $framework->name }} <br>
                            @endforeach
                    </p>
                    </a>
                </div>
            </div>
           
        </div>
        @endforeach

    </div>
</div>
