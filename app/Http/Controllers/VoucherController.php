<?php

namespace App\Http\Controllers;

use App\Models\Kendaraan;
use App\Models\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vouchers =Voucher::all();
        return view('voucher.index',['vouchers'=> $vouchers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kendaraans = Kendaraan::all();
        return view('voucher.create',['kendaraans'=> $kendaraans]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nik' => 'required|unique:vouchers',
            'name' => 'required',
            'plat' => 'required',
        ]);
        $voucher =Voucher::create($request->all());
        $voucher->kendaraan()->sync($request->kendaraans);
        if( $voucher ['jarak'] >= 15000){
            return view('voucher.congrats');
        }
        else{
            return view('voucher.sorry');
        }
       
    }
            
        public function test()
        { $voucher=Voucher::all();
            return view('voucher.show',['voucher'=> $voucher]);
        }
         
    
    


    
 
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function myvoucher($jarak)
    {
        $voucher = Voucher::where('jarak', $jarak);
if($voucher >= 15000) {
    return view('voucher.congratulation');
} else {
    return view('voucher.sorry');
}
    }
    public function show(Voucher $voucher,$id)
    {
        $kendaraan = Kendaraan::where ('id', $id)->first();
        $voucher = Voucher::where ('id', $id)->first();
        if(voucher ::where('jarak' >= 15000)){
            return view('voucher.congratulation');
        }
        else{
            return view('voucher.sorry');
        }
        return view('voucher.show',['voucher' => $voucher,'kendaraan' => $kendaraan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        //
    }
}